﻿<UserControl
    x:Class="NINA.View.OptionsImagingView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:enum="clr-namespace:NINA.Utility.Enum"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:rules="clr-namespace:NINA.Utility.ValidationRules"
    xmlns:util="clr-namespace:NINA.Utility"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <UniformGrid Columns="2">
        <GroupBox Header="{ns:Loc LblFileSettings}">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="100" />
                    <ColumnDefinition Width="3*" />
                    <ColumnDefinition Width="50" />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height="40" />
                    <RowDefinition Height="25" />
                    <RowDefinition Height="40" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <TextBlock
                    Grid.Column="0"
                    VerticalAlignment="Center"
                    Text="{ns:Loc LblSaveImageAs}" />
                <ComboBox
                    Grid.Column="1"
                    ItemsSource="{Binding FileTypes}"
                    SelectedItem="{Binding FileType}" />
                <StackPanel
                    Grid.Row="1"
                    Grid.ColumnSpan="3"
                    Orientation="Horizontal">
                    <Path
                        Width="25"
                        Margin="5"
                        Data="{StaticResource ExclamationCircledSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                    <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblSaveImageAsDSLRNote}" />
                </StackPanel>

                <TextBlock
                    Grid.Row="2"
                    Grid.Column="0"
                    VerticalAlignment="Center"
                    Text="{ns:Loc LblImageFilePath}" />
                <TextBox
                    Grid.Row="2"
                    Grid.Column="1"
                    VerticalAlignment="Center"
                    Text="{Binding ImageFilePath}" />
                <Button
                    Grid.Row="2"
                    Grid.Column="2"
                    Width="30"
                    Height="30"
                    HorizontalAlignment="Center"
                    VerticalAlignment="Center"
                    Command="{Binding OpenImageFileDiagCommand}">
                    <Path
                        Margin="4,17,4,4"
                        Data="{StaticResource DotsSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                </Button>

                <TextBlock
                    Grid.Row="3"
                    Grid.Column="0"
                    VerticalAlignment="Center"
                    Text="{ns:Loc LblImageFilePattern}" />
                <TextBox
                    x:Name="ImageFilePatternTextBox"
                    Grid.Row="3"
                    Grid.Column="1"
                    VerticalAlignment="Center"
                    AllowDrop="True"
                    DragEnter="TextBox_DragEnter"
                    Drop="TextBox_Drop"
                    PreviewDragOver="TextBox_PreviewDragOver"
                    Text="{Binding ImageFilePattern}"
                    TextWrapping="Wrap" />
                <Button
                    Grid.Row="3"
                    Grid.Column="2"
                    Width="30"
                    Height="30"
                    HorizontalAlignment="Center"
                    VerticalAlignment="Center"
                    Command="{Binding PreviewFileCommand}">
                    <Path
                        Margin="5"
                        Data="{StaticResource PreviewSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                </Button>

                <ListView
                    x:Name="ImagePatternList"
                    Grid.Row="4"
                    Grid.ColumnSpan="3"
                    Margin="0,5,0,0"
                    ItemsSource="{Binding ImagePatterns.Items}">
                    <ListView.View>
                        <GridView>
                            <GridViewColumn
                                Width="Auto"
                                DisplayMemberBinding="{Binding Key}"
                                Header="{ns:Loc LblPatternName}" />
                            <GridViewColumn
                                Width="Auto"
                                DisplayMemberBinding="{Binding Description}"
                                Header="{ns:Loc LblDescription}" />
                        </GridView>
                    </ListView.View>

                    <ListView.ItemContainerStyle>
                        <Style BasedOn="{StaticResource ListViewItemStyle}" TargetType="ListViewItem">
                            <EventSetter Event="PreviewMouseLeftButtonDown" Handler="ListViewItem_PreviewMouseLeftButtonDown" />
                            <EventSetter Event="PreviewMouseDoubleClick" Handler="ListViewItem_PreviewMouseDoubleClick" />
                        </Style>
                    </ListView.ItemContainerStyle>
                </ListView>
            </Grid>
        </GroupBox>
        <StackPanel Orientation="Vertical">
            <GroupBox Header="{ns:Loc LblAutoMeridianFlip}">
                <StackPanel Orientation="Vertical">
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblEnabled}" />
                        <CheckBox
                            Width="75"
                            Height="25"
                            HorizontalAlignment="Left"
                            IsChecked="{Binding AutoMeridianFlip}" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblMinutesAfterMeridian}" />
                        <TextBox
                            MinWidth="75"
                            MinHeight="20"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Path="MinutesAfterMeridian" UpdateSourceTrigger="LostFocus">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblRecenterAfterFlip}" />
                        <CheckBox
                            Width="75"
                            Height="25"
                            HorizontalAlignment="Left"
                            IsChecked="{Binding RecenterAfterFlip}" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblScopeSettleTimeAfterFlip}" />
                        <TextBox
                            MinWidth="75"
                            MinHeight="20"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Path="MeridianFlipSettleTime" UpdateSourceTrigger="LostFocus">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblPauseTimeBeforeMeridian}" />
                        <TextBox
                            MinWidth="75"
                            MinHeight="20"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Path="PauseTimeBeforeMeridian" UpdateSourceTrigger="LostFocus">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                </StackPanel>
            </GroupBox>
            <GroupBox Header="{ns:Loc LblImageOptions}">
                <StackPanel Orientation="Vertical">
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblAutoStretchFactor}">
                            <TextBlock.ToolTip>
                                <TextBlock Text="{ns:Loc LblAutoStretchFactorTooltip}" />
                            </TextBlock.ToolTip>
                        </TextBlock>
                        <TextBox
                            MinWidth="75"
                            MinHeight="20"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Path="AutoStretchFactor" UpdateSourceTrigger="LostFocus">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblAnnotateImage}">
                            <TextBlock.ToolTip>
                                <TextBlock Text="{ns:Loc LblAnnotateImageTooltip}" />
                            </TextBlock.ToolTip>
                        </TextBlock>
                        <CheckBox
                            Width="75"
                            Height="25"
                            HorizontalAlignment="Left"
                            IsChecked="{Binding AnnotateImage}" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblDebayerImage}">
                            <TextBlock.ToolTip>
                                <TextBlock Text="{ns:Loc LblDebayerImageTooltip}" />
                            </TextBlock.ToolTip>
                        </TextBlock>
                        <CheckBox
                            Width="75"
                            Height="25"
                            HorizontalAlignment="Left"
                            IsChecked="{Binding DebayerImage}" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="200"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblHistogramResolution}">
                            <TextBlock.ToolTip>
                                <TextBlock Text="{ns:Loc LblHistogramResolutionTooltip}" />
                            </TextBlock.ToolTip>
                        </TextBlock>
                        <TextBox
                            MinWidth="75"
                            MinHeight="20"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Path="HistogramResolution" UpdateSourceTrigger="LostFocus">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                </StackPanel>
            </GroupBox>
            <GroupBox Header="{ns:Loc LblSequence}">
                <StackPanel Orientation="Vertical">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="200" />
                            <ColumnDefinition Width="3*" />
                            <ColumnDefinition Width="50" />
                        </Grid.ColumnDefinitions>
                        <TextBlock
                            Grid.Column="0"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblSequenceTemplate}" />
                        <TextBox
                            Grid.Column="1"
                            VerticalAlignment="Center"
                            Text="{Binding SequenceTemplatePath}" />
                        <Button
                            Grid.Column="2"
                            Width="30"
                            Height="30"
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Command="{Binding OpenSequenceTemplateDiagCommand}">
                            <Path
                                Margin="4,17,4,4"
                                Data="{StaticResource DotsSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                            <Button.ToolTip>
                                <TextBlock Text="{ns:Loc LblSequenceTemplateTooltip}" />
                            </Button.ToolTip>
                        </Button>
                    </Grid>
                </StackPanel>
            </GroupBox>
        </StackPanel>
    </UniformGrid>
</UserControl>